CC=g++

DEBUG=false
VERBOSE=true

# CFLAGS=-Wall -Werror -fno-strict-aliasing
# CXXFLAGS=-Wall -Werror -fno-strict-aliasing
# LDFLAGS=-Wl,-static -Wl,-call_shared -lm -lc 
LDFLAGS=-lwsock32

ifeq ($(DEBUG), true)
	CFLAGS+=-ggdb #-D_DEBUG
else
	CFLAGS+=-O3 -fvisibility=hidden
	LDFLAGS+=-Wl,-O2 -s
endif

ifneq ($(VERBOSE), true)
	CC:=@$(CC)
endif

SRCS=$(wildcard *.c *.cc *.cpp)
OBJS=$(patsubst %.c,%.o,$(patsubst %.cc,%o,$(patsubst %.cpp,%.o,$(SRCS))))
BINS=server client
EXES=$(patsubst %,%.exe,$(BINS))

%.o:%.c
	$(CC) -o $@ $^ -c $(CFLAGS)

%.o:%.cc
	$(CC) -o $@ $^ -c $(CXXFLAGS)

%:%.o $(BASEOBJS)
	$(CC) -o $@ $^ $(LDFLAGS)
	
all:$(OBJS) $(BINS)

clean:
	rm -f $(EXES) $(BINS) $(OBJS)

tags:
	if [ -f $(HOME)/tags ]; then rm $(HOME)/tags; fi
	ctags -f $(HOME)/tags -aR --c-types=+px $(HOME)/*

.PHONY:all release install clean tags

