#include <stdio.h>
#include <winsock2.h>

int main(int argc, char* argv[]) {
	char lhost[20] = "127.0.0.1";
	int lport = 2222;
	if (argc > 1) {
		strcpy(lhost, argv[1]);
	}
	if (argc > 2) {
		lport = atoi(argv[2]);
	}

    WORD sockver = MAKEWORD(2,2);
    WSADATA wsadata;
    if(WSAStartup(sockver, &wsadata) != 0) {
        return 0;
    }

    SOCKET sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP); 
    if(sock == INVALID_SOCKET) {
        printf("socket error !");
        return 0;
    }

    sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(lport);
    addr.sin_addr.S_un.S_addr = inet_addr(lhost);
    if(bind(sock, (sockaddr *) &addr, sizeof(addr)) == SOCKET_ERROR) {
        printf("bind error !");
        closesocket(sock);
        return 0;
    }
	
	printf("start to listen %s\r\n", lhost);
    
    sockaddr_in raddr;
    int raddrlen = sizeof(raddr); 
    while (true) {
        char rbuf[256];  
        int rlen = recvfrom(sock, rbuf, sizeof(rbuf), 0, (sockaddr *) &raddr, &raddrlen);
        if (rlen > 0) {
            rbuf[rlen] = '\0';
            printf("recv[%s]: %s\n", inet_ntoa(raddr.sin_addr), rbuf);           
        } else {
			printf("recv fail!\n");
		}

        char sbuf[256];
		snprintf(sbuf, sizeof(sbuf), "hello, this is server %s", lhost);
        sendto(sock, sbuf, strlen(sbuf), 0, (sockaddr *) &raddr, raddrlen);    
    }
    closesocket(sock); 
    WSACleanup();
	
    return 0;
}
