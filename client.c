#include <stdio.h>
#include <winsock2.h>

int main(int argc, char* argv[]) {
	char rhost[20] = "127.0.0.1";
	int rport = 2222;
	if (argc > 1) {
		strcpy(rhost, argv[1]);
	}
	if (argc > 2) {
		rport = atoi(argv[2]);
	}

    WORD sockver = MAKEWORD(2,2);
    WSADATA wsadata; 
    if(WSAStartup(sockver, &wsadata) != 0) {
        return 0;
    }
    SOCKET sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    
    sockaddr_in addr;
    addr.sin_family = AF_INET;
	addr.sin_addr.S_un.S_addr = inet_addr(rhost);
    addr.sin_port = htons(rport);
    int addrlen = sizeof(addr);
    
    char sbuf[256];
	snprintf(sbuf, sizeof(sbuf), "hello, %s", rhost);
    sendto(sock, sbuf, strlen(sbuf), 0, (sockaddr *) &addr, addrlen);

    char rbuf[256];     
    int rlen = recvfrom(sock, rbuf, sizeof(rbuf), 0, (sockaddr *) &addr, &addrlen);
    if(rlen > 0) {
        rbuf[rlen] = '\0';
        printf("recv[%s]: %s\n", inet_ntoa(addr.sin_addr), rbuf);   
    } else {
		printf("recv fail!\n");
	}

    closesocket(sock);
    WSACleanup();
	
    return 0;
}

